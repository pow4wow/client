## What is this for?

You are looking at client package for PoW protected server.
This package allows you to connect to the server and receive useless wisdom quotes.

### How to build image:

```
make docker-build 
```

### How build it:

```
make build
```

### How to run it:

After you've built it, just launch the binary

```
./bin/client {host:port}
```

#### Don't forget

to provide server address `host:port` as first argument 

