CLIENT_DOCKER_IMAGE_NAME = "pow4wow-client"
DOCKER_IMAGE_TAG = "latest"

#GOPRIVATE ?= "gitlab.com/pow4wow/*"

.PHONY: docker-build
docker-build:
	docker build -t $(CLIENT_DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) --build-arg GOPRIVATE=$(GOPRIVATE) --build-arg GIT_USER=$(GIT_USER) --build-arg GIT_PASSWORD=$(GIT_PASSWORD) .

.PHONY: deps
deps:
	go mod download

.PHONY: build
build: deps
	GO111MODULE=on go build -o bin/client .
