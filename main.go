package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"
	"time"

	"gitlab.com/pow4wow/common"
	"gitlab.com/pow4wow/pow"
)

func main() {
	if len(os.Args) == 1 {
		fmt.Println("no server address provided")
		os.Exit(1)
	}

	cn, err := net.Dial("tcp", os.Args[1])
	if err != nil {
		fmt.Printf("couldn't connect to server: %v", err)
		os.Exit(1)
	}

	handleConnection(cn)
}

func handleConnection(conn net.Conn) {
	defer func() {
		fmt.Println("closing connection")
		if err := conn.Close(); err != nil {
			fmt.Printf("can't close connection: %v\n", err)
		}
	}()

	incomingChan := make(chan []byte)
	outgoingChan := make(chan []byte)
	stopChan := make(chan struct{})
	errChan := make(chan error)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)

	defer cancel()

	go common.WaitForMessage(ctx, conn, incomingChan, errChan, stopChan)
	go common.WriteToChannel(ctx, conn, outgoingChan, errChan, stopChan)

	requestServiceMessage, err := common.PrepareMessage(common.ClientRequestServiceHeader, "")
	if err != nil {
		panic(err)
	}

	outgoingChan <- requestServiceMessage

	for {
		select {
		case msg := <-incomingChan:
			fmt.Printf("received message: %s\n", msg)
			go processIncomingMessage(msg, outgoingChan, errChan, stopChan)
		case <-stopChan:
			fmt.Println("stop signal received")
			return
		case err := <-errChan:
			fmt.Printf("closing connection with error: %v", err)
			return
		}
	}
}

func processIncomingMessage(msg []byte, outgoingChan chan<- []byte, errChan chan<- error, stopChan chan<- struct{}) {
	header, messageValue, err := common.GetMessageValues(msg)
	if err != nil {
		errChan <- err
		return
	}

	switch header {
	case common.ServerChallengeProvidedHeader:
		messageValues := strings.Split(messageValue, common.MessageSeparator)
		if len(messageValues) < 2 {
			errChan <- fmt.Errorf("invalid challenge payload: %s", messageValue)
			return
		}

		prover := pow.NewProver(pow.WithBoundData([]byte(messageValues[1])))
		proof, err := prover.DoWork(messageValues[0])
		if err != nil {
			errChan <- fmt.Errorf("can't perform proof work: %v", err)
			return
		}

		outgoingMessage, err := common.PrepareMessage(common.ClientChallengeResponseHeader, proof)
		if err != nil {
			errChan <- err
			return
		}

		outgoingChan <- outgoingMessage
	case common.ServerWordOfWisdomResponse:
		fmt.Printf("Received wisdom: %s\n", messageValue)
		stopChan <- struct{}{}
	default:
		fmt.Printf("unexpected response: %s\n", messageValue)
		errChan <- fmt.Errorf("closing connection")
	}
}
