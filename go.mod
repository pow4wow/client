module gitlab.com/pow4wow/client

go 1.16

require (
	gitlab.com/pow4wow/common v0.0.3
	gitlab.com/pow4wow/pow v0.0.1
)
